# 1.0.1

* Pin `poetry-core` to avoid "empty requirements.txt" issue

# 1.0.0

* Updated README to better clarify purpose of `pip-compile-cross-platform`
* Added automated integration tests to ensure stability

# 0.9.3

* Add support for `setup.py` input files
* Add support for `requirements.in`-format input files that link to other projects on the filesystem
* Improve output to be less noisy and more descriptive.

# 0.9.2

Fix "No module named 'tomli'" error

# 0.9.1

Support reusing existing lockfile, causing minimal changes

# 0.9.0

Initial release
